const express = require('express');
const sendEmail =require('./src/controller/email');
const bodyParser = require('body-parser');
const config = require('./src/config/production/emailTemplates');

const app = express();
const port = 8080;

app.use(bodyParser.json());


//TODO: Verify request 
app.use('/email', async(req, res)=>{
    try{
        console.log(config);
        const response = await sendEmail(req.body);
        if(response.error){
            res.status(500).send(response);
        }else{
            res.status(200).send(response);
        }
    }catch(err){
        res.status(500).send(err);
    }
});

app.listen(port, err=>{
    if(err){
        return console.error(err);
    }
    return console.log(`Server is running in port ${port}`);
});