const nodemailer = require("nodemailer");
require('dotenv').config();
const ejs = require('ejs');
const sgMail = require('@sendgrid/mail');


const sendEmail = async (body) =>{
    try{
        console.log(process.env.SENDGRID_API_KEY);
        sgMail.setApiKey(process.env.SENDGRID_API_KEY);
        const email = await new Promise((resolve, reject)=>{
            ejs.renderFile(`${__dirname}/../templates/Welcome/index.ejs`, {...body},(err, data) =>{
                if(err){
                    return reject(err);
                } else {
                    return resolve(data);
                }
            } );
        });
        // console.log(email);
        console.log(process.env.EMAIL_ACC);
        const msg = {
            to: body.to,
            from: process.env.EMAIL_ACC,
            subject:  'Test de serverless email service',
            text: 'and easy to do anywhere, even with Node.js',
            html: email,
          };
        sgMail.send(msg);
        //TODO: Move data from this
        //TODO: Make this async tasks cleaner


        return {};
    }catch ( err){
        return {
            error: true,
            message: err.message
        };
    }
}
module.exports =  sendEmail;